Categories:Navigation
License:Apache-2.0
Web Site:http://members.iinet.net.au/~ninelima/efis/
Source Code:https://github.com/ninelima/kwikEFIS
Issue Tracker:https://github.com/ninelima/kwikEFIS/issues
Bitcoin:1KKWRF25NwVgNdankr1vBphtkLbX766Ee1

Auto Name:Kwik EFIS
Summary:Electronic Flight Information System (EFIS)
Description:
Kwik EFIS is a Glass Cockpit application designed to work on most  Android
devices equipped with a GPS, gyroscope, accelerometer and a  CPU with reasonable
performance.

* Flight Director

Kwik EFIS has a fully functional flight director built in. It uses  the standard
V-Bar symbology common to modern flight directors.  The target waypoint and
altitude is set on-screen by means of the  spinner controls on the right top and
bottom of the screen.

* Demo Mode

There is also a "Demo Mode" available in the application. It is fairly
rudimentary and works like a crude flight simulator.  The heading and  altitude
can be changed by pitching and banking the device. The speed  runs up and down
automatically based on the pitch.

* [http://members.iinet.net.au/~ninelima/efis/gallery.html Screenshots]
.

Repo Type:git
Repo:https://github.com/ninelima/kwikEFIS

Build:2.2.0,3
    commit=42b6ea0131afd1cb2fa1ea4de2e1eb70258bb602
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.2.0
Current Version Code:3
